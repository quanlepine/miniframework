﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameManagerController : Singleton<GameManagerController>
{
    public enum GAMESTATE
    {
        GAMEPLAY,
        GAMEOVER
    }

    public int Life;   

    private GAMESTATE _State;
    private int _CurrentLife;
    private int _CurrentScores;

    // Use this for initialization
    void Start()
    {
        GameplayGuiController.Instance.Initialize();
        BalloonManager.Instance.Initialize();
        TouchController.Instance.Initialize();
    }

    // Update is called once per frame
    void Update()
    {
        if (_State == GAMESTATE.GAMEPLAY)
        {
            BalloonManager.Instance.UpdateBalloonManager();
            TouchController.Instance.UpdateTouchController();
        }        
    }

    void OnDestroy()
    {        
        System.GC.Collect();
    }

    public void StartGame()
    {
        _State = GAMESTATE.GAMEPLAY;
    }

    public void TouchOnBalloon(Balloon balloon)
    {
        _CurrentScores += balloon.Scores;
        GameplayGuiController.Instance.UpdateScoreText(_CurrentScores);
        BalloonManager.Instance.DestroyBalloon(balloon, true);        
    }

    public void LostBalloon()
    {
        _CurrentLife -= 1;
        if (_CurrentLife <= 0)
        {
            _State = GAMESTATE.GAMEOVER;
            GameplayGuiController.Instance.DisplayGameplayGui(false);
            SceneManager.LoadScene("GameOver", LoadSceneMode.Additive);

            BalloonManager.Instance.DestroyAllBalloon();            
        }
    }

    public void RetryGame()
    {
        GameplayGuiController.Instance.DisplayGameplayGui(true);
        GameOverController.Instance.DestroySingleton();

        _CurrentLife = Life;
        _State = GAMESTATE.GAMEPLAY;
        _CurrentScores = 0;
        GameplayGuiController.Instance.UpdateScoreText(_CurrentScores);

        SceneManager.UnloadScene("GameOver");
    }

    public void ReturnMenu()
    {
        Destroy(GameplayGuiController.Instance.gameObject);
        Destroy(GameOverController.Instance.gameObject);

        Destroy(PoolingSystem.Instance.gameObject);
        Destroy(TouchController.Instance.gameObject);
        Destroy(BalloonManager.Instance.gameObject);

        SceneManager.LoadScene("Menu", LoadSceneMode.Single);
        Destroy(gameObject);

        SceneManager.UnloadScene("MiniGame");
    }
}
