﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour
{
    public RectTransform Title;
    public RectTransform PlayBtn;

    public Vector3 _OriginTitlePos;
    public Vector3 _OriginPlayBtnPos;

    void Start()
    {
        _OriginTitlePos = Title.position;
        _OriginPlayBtnPos = PlayBtn.position;

        HideMenu(false, false);
        ShowMenu(true);
    }

    void OnDestroy()
    {
        System.GC.Collect();
    }

    public void OnPlayBtnPress()
    {
        HideMenu(true);
    }

    private void ShowMenu(bool isAnim)
    {
        if (isAnim)
        {
            LeanTween.moveY(Title.gameObject, _OriginTitlePos.y, 0.5f)
                .setEase(LeanTweenType.easeOutBack);

            LeanTween.moveY(PlayBtn.gameObject, _OriginPlayBtnPos.y, 0.75f)
                .setEase(LeanTweenType.easeInCubic)
                .setOnComplete(() => {
                    LeanTween.moveY(PlayBtn.gameObject, _OriginPlayBtnPos.y + 10f, 0.5f)
                        .setEase(LeanTweenType.linear)
                        .setLoopPingPong(-1);
                });

        }
        else
        {
            Title.position = _OriginTitlePos;
            PlayBtn.position = _OriginPlayBtnPos;

            LeanTween.moveY(PlayBtn.gameObject, _OriginPlayBtnPos.y + 0.2f, 0.5f)
                        .setEase(LeanTweenType.linear)
                        .setLoopPingPong(-1);
        }
    }

    private void HideMenu(bool isAnim, bool isQuit = true)
    {
        if (isAnim)
        {
            LeanTween.moveY(Title.gameObject, _OriginTitlePos.y + 150f, 0.5f)
                .setEase(LeanTweenType.easeOutBack);

            LeanTween.cancel(PlayBtn.gameObject);
            LeanTween.moveY(PlayBtn.gameObject, _OriginPlayBtnPos.y - 350f, 0.75f)
                .setEase(LeanTweenType.easeInCubic)
                .setOnComplete(() => {
                    //LeanTween.cancel(Title.gameObject);
                    //LeanTween.cancel(PlayBtn.gameObject);

                    //if (isQuit)
                    //{
                    //    DestroyObject(gameObject);
                    //}

                    StartCoroutine(LoadGameScene());
                });
        }
        else
        {
            Title.SetPositionY(_OriginTitlePos.y + 150f);
            PlayBtn.SetPositionY(_OriginPlayBtnPos.y - 350f);

            //LeanTween.cancel(Title.gameObject);
            //LeanTween.cancel(PlayBtn.gameObject);

            //if (isQuit)
            //{
            //    DestroyObject(gameObject);
            //}
        }
    }

    private IEnumerator LoadGameScene()
    {
        AsyncOperation async = SceneManager.LoadSceneAsync("MiniGame", LoadSceneMode.Single);

        while (!async.isDone)
        {
            // do something (display loading icon and animation...)

            yield return null;
        }
    }
}
