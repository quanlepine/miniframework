﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Text;

public class GameplayGuiController : Singleton<GameplayGuiController>
{
    public RectTransform GameplayGuiPanel;
    public Text ScoreTxt;

    public void Initialize()
    {

    }

    public void DisplayGameplayGui(bool isDisplay)
    {
        GameplayGuiPanel.gameObject.SetActive(isDisplay);
    }

    public void UpdateScoreText(int scores)
    {
        StringBuilder strb = new StringBuilder("Scores: ");
        strb.Append(scores);

        ScoreTxt.text = strb.ToString();
    }
}
