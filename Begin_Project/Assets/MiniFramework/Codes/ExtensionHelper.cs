﻿using UnityEngine;
using System.Collections;

public static class ExtensionHelper
{
    public static Vector3 TempVector3;

    public static void SetPositionX(this Transform transform, float x)
    {
        TempVector3 = transform.position;
        TempVector3.x = x;
        transform.position = TempVector3;
        //transform.position = new Vector3(x, transform.position.y, transform.position.z);
    }

    public static void SetPositionY(this Transform transform, float y)
    {
        TempVector3 = transform.position;
        TempVector3.y = y;
        transform.position = TempVector3;
        //transform.position = new Vector3(transform.position.x, y, transform.position.z);
    }

    public static void SetPositionZ(this Transform transform, float z)
    {
        TempVector3 = transform.position;
        TempVector3.z = z;
        transform.position = TempVector3;
        //transform.position = new Vector3(transform.position.x, transform.position.y, z);
    }

    public static void SetLocalScaleX(this Transform transform, float x)
    {
        transform.localScale = new Vector3(x, transform.localScale.y, transform.localScale.z);
    }

    public static void SetLocalScaleY(this Transform transform, float y)
    {
        transform.localScale = new Vector3(transform.localScale.x, y, transform.localScale.z);
    }

    public static void SetLocalScaleZ(this Transform transform, float z)
    {
        transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y, z);
    }
}
