﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BalloonManager : Singleton<BalloonManager>
{
    public float SpawnTime;

    [Range(0, 100)]
    public int RegularPercent;
    [Range(0, 100)]
    public int RarePercent;
    [Range(0, 100)]
    public int VeryRarePercent;

    private float _Time;
    private List<Balloon> _BalloonList;

    public void Initialize()
    {
        _Time = 0;
        _BalloonList = new List<Balloon>();
    }

    public void UpdateBalloonManager()
    {
        _Time += Time.deltaTime;
        if (_Time >= SpawnTime)
        {
            int percent = Random.Range(1, 101);
            string balloonName = "Balloon_Green";
            if (percent <= VeryRarePercent)
            {
                balloonName = "Balloon_Red";
            }
            else if (percent <= RarePercent)
            {
                balloonName = "Balloon_Bundles";
            }

            SpawnBalloon(balloonName);

            _Time -= SpawnTime;
        }

        for (int i = 0; i < _BalloonList.Count; i++)
        {
            _BalloonList[i].UpdateBalloon();

            // out of screen's limit
            if (_BalloonList[i].GetTranform().position.y >= _BalloonList[i].DesPos.y - 0.01f)
            {
                DestroyBalloon(_BalloonList[i], false);
                GameManagerController.Instance.LostBalloon();
            }
        }
    }

    private void SpawnBalloon(string balloonName)
    {
        Balloon balloon = PoolingSystem.Instance.InstantiateAPS(balloonName).GetComponent<Balloon>();
        _BalloonList.Add(balloon);

        balloon.Initialize();
    }

    public void DestroyBalloon(Balloon balloon, bool isAnim)
    {
        _BalloonList.Remove(balloon);
        balloon.DestroyBalloon(isAnim);
    }

    public void DestroyAllBalloon()
    {
        while (_BalloonList.Count > 0)
        {
            Balloon balloon = _BalloonList[0];
            _BalloonList.RemoveAt(0);

            DestroyBalloon(balloon, false);
        }
    }
}
