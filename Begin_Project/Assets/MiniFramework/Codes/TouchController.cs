﻿using UnityEngine;
using System.Collections;

public class TouchController : Singleton<TouchController>
{
    public void Initialize()
    {

    }

    public void UpdateTouchController()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit2D raycastHit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
            if (raycastHit)
            {
                GameManagerController.Instance.TouchOnBalloon(raycastHit.collider.gameObject.GetComponent<Balloon>());
            }
        }
    }
}
