﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(SpriteRenderer))]
public class Balloon : MonoBehaviour
{
    public int Scores;
    public float Speed;

    private Transform _Transform;
    private Vector3 _OriginPos;
    private Vector3 _DesPos;
    private float _Duration;
    private float _Time;

    public Vector3 DesPos
    {
        get { return _DesPos; }
    }

    public Transform GetTranform()
    {
        return _Transform;
    }

    public void Initialize()
    {
        _Transform = gameObject.transform;
        _OriginPos = _DesPos = _Transform.position = new Vector3(Random.Range(-2f, 2f), -10f, 0f);
        _DesPos.y = 15f;
        _Duration = (_DesPos.y - _OriginPos.y) / Speed;
        _Time = 0;
    }

    public void UpdateBalloon()
    {
        float newPosY = (_DesPos.y - _OriginPos.y) * _Time / _Duration + _OriginPos.y;
        transform.SetPositionY(newPosY);

        _Time += Time.deltaTime;
    }

    public void DestroyBalloon(bool isAnimted)
    {
        if (isAnimted)
        {
            // TODO: show effect balloon explosion
            gameObject.DestroyAPS();
        }
        else
        {
            gameObject.DestroyAPS();
        }
    }
}

