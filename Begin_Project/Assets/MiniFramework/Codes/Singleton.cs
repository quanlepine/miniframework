﻿using UnityEngine;
using System.Collections;

public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
    public static T Instance;

    protected virtual void Awake()
    {
        if (Instance == null)
        {
            Instance = this as T;

            GameObject.DontDestroyOnLoad(this);
        }
        else
        {
            Debug.Log("<>==### The Singletone: " + typeof(T).ToString() + " was duplicate!!");
            DestroySingleton();
        }
    }

    public void DestroySingleton()
    {
        Destroy(gameObject);
    }
}
