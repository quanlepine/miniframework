﻿using UnityEngine;
using System.Collections;

public class GameOverController : Singleton<GameOverController>
{
    public RectTransform GameOverPanel;
    
    void Start ()
    {

    }

    public void ShowGUI()
    {
        GameOverPanel.gameObject.SetActive(true);
    }

    public void HideGUI()
    {
        GameOverPanel.gameObject.SetActive(false);
    }

    public void OnRetryTouch()
    {
        GameManagerController.Instance.RetryGame();
    }

    public void OnMenuTouch()
    {
        GameManagerController.Instance.ReturnMenu();
    }
}
