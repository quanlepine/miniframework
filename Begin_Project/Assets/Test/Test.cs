﻿using UnityEngine;
using System.Collections;

public class Test : MonoBehaviour
{
    public GameObject TestSphere;

    private GameObject[] TestGameObjects;

    private Vector3 _Velocity;

	// Use this for initialization
	void Start ()
    {
        TestGameObjects = new GameObject[1000];
        for (int i = 0; i < 1000; i++)
        {            
            TestGameObjects[i] = Instantiate(TestSphere, Vector3.zero, Quaternion.identity) as GameObject;
        }
	}
	
	// Update is called once per frame
	void Update ()
    {
	    for (int i = 0; i < TestGameObjects.Length; i++)
        {
            TestGameObjects[i].transform.SetPositionX(TestGameObjects[i].transform.position.x + Random.Range(-0.3f, 0.3f));
            TestGameObjects[i].transform.SetPositionY(TestGameObjects[i].transform.position.y + Random.Range(-0.3f, 0.3f));
            if (TestGameObjects[i].transform.position.x > 3f || TestGameObjects[i].transform.position.x < -3f ||
                TestGameObjects[i].transform.position.y > 5f || TestGameObjects[i].transform.position.y < -5f)
            {
                TestGameObjects[i].transform.SetPositionX(0);
                TestGameObjects[i].transform.SetPositionY(0);
            }
        }
	}
}
